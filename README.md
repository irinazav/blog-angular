------------------------------------------------

# **BLOG (under construction)** ##

Standalone blog with Admin based on [multi-blogging project "LJ"](https://bitbucket.org/irinazav/lj)

------------------------------------------------
 
Two front-ends share the same back-end:


**1) Front end  - Razor, jQuery**  
[blog2017-001-site1.itempurl.com](http://blog2017-001-site1.itempurl.com)

**2) Front end - Angular**  
[blog2017-001-site1.itempurl.com/blog.html#!/posts](http://blog2017-001-site1.itempurl.com/blog.html#!/posts)

### Used software: ###

**Languages**

* C# , JavaScript

**Internet**

* ASP.NET MVC 5

**Front end - jQuery**

*  Razor, jQuery, Ajax, jQuery Templates,  Bootstrap 

**Front end - Angular**

* Angular,  [UI-Router](https://ui-router.github.io/ng1/docs/0.3.1/index.html#/api/ui.router), 
[UI-tinyMce](https://github.com/angular-ui/ui-tinymce),
[UI-Bootstrap](https://angular-ui.github.io/bootstrap/) 


**Plugins** 

* [tinyMCE](https://www.tinymce.com), advanced HTML editor
* [highCharts](http://www.highcharts.com/demo), a charting library
* [highMaps](http://www.highcharts.com/maps/demo), a map library

**API**

* [freeGeoIP](https://freegeoip.net/), public HTTP API to search the geolocation of IP addresses

**Database**

* MS SQL Server 2014

**ORM**

* Entity Framework 6 (Code First)

**Background processing**

* [Hangfire](http://hangfire.io/)

------------------------------------------------

# **Screenshots** #



## **Posts (front end - Razor, jQuery)**

------------------------------------------------

![BlogA1.png](https://bitbucket.org/irinazav/blog-angular/downloads/BlogA1.png)

------------------------------------------------
## **Posts  (front end - Angular)**

------------------------------------------------

![Blog2.png](https://bitbucket.org/irinazav/blog-angular/downloads/Blog2.png)

------------------------------------------------




## **Edit post (front end - Razor, jQuery)**


------------------------------------------------

![Blog4.png](https://bitbucket.org/irinazav/blog-angular/downloads/Blog4.png)

------------------------------------------------

## **Edit post  (front end - Angular)**

------------------------------------------------

![BlogA3.png](https://bitbucket.org/irinazav/blog-angular/downloads/BlogA3.png)

------------------------------------------------


## **Admin (front end - Razor, jQuery)**

*Manage tags, posts*

------------------------------------------------
 
![Blog3.jpg](https://bitbucket.org/irinazav/blog-angular/downloads/Blog3.jpg)

------------------------------------------------

## **Admin  (front end - Angular)**

*Manage tags, posts, blog pages: "comtact", "about"*

------------------------------------------------
 
![BlogA2.png](https://bitbucket.org/irinazav/blog-angular/downloads/BlogA2.png)



------------------------------------------------




## **Visitors chart (front end - Razor, jQuery)**

------------------------------------------------

![Blog5.jpg](https://bitbucket.org/irinazav/blog-angular/downloads/Blog5.jpg)

------------------------------------------------

## **Visitors map (front end - Razor, jQuery)**

------------------------------------------------

![4236017340-report3%5B1%5D.jpg](https://bitbucket.org/repo/dabgxnq/images/4236017340-report3%5B1%5D.jpg)

------------------------------------------------