﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models.Visitor
{
    public class GeoLocation
    {
        public string ip { get; set; }
        public string country_code { get; set; }
        public string country_name { get; set; }
        public string region_code { get; set; }
        public string city { get; set; }
        public string zip_code { get; set; }
        public decimal latitude { get; set; }
        public decimal longitude { get; set; }
        public int metro_code { get; set; }
    }
}