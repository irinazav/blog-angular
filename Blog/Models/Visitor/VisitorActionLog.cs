﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;


namespace Blog.Models.Visitor
{
    public class VisitorActionLog
    {
        public Int64 Id { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public int ActionType { get; set; }
        public string IpAddress { get; set; }
        public DateTime DateTime { get; set; }
        public string VisitorUserName { get; set; }
        public string Anonymous { get; set; }
        public string SessionId { get; set; }
        public int PostId { get; set; }
        public int TagId { get; set; }

        [NotMapped]
        public TimeSpan SpentTime
        {
            get;
            set;
        }

        public long SpentTimeMC
        {
            get
            {
                return SpentTime.Ticks;
            }
            set
            {
                SpentTime = TimeSpan.FromTicks(value);
            }
        }
    }
}