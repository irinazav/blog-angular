﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models.Visitor
{
    public class Country
    {
        public string Id { get; set; }
        public string Name { get; set; }
       
        virtual public IEnumerable<WebVisitor> Visitors { get; set; }
    }
}