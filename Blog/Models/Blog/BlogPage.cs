﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Blog.Models.Blog
{
    public class BlogPage
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string Content { get; set; }
    }
}