﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
//using LJ.Models.User;

namespace Blog.Models.Blog
{
    public class Post
    {

        public int Id { get; set; }

        [Required(ErrorMessage = " is required")]
        [StringLength(200, ErrorMessage = " Length should not exceed 200 characters")]
        public string Title
        { get; set; }

        [Display(Name = "Short Description")]
        [Required(ErrorMessage = "Short Description: Field is required")]
        public string Description
        { get; set; }


        [Display(Name = "Content")]
        [Required(ErrorMessage = "Content: Field is required")]
        public virtual string Body
        { get; set; }

        public bool Published
        { get; set; }

        // [Required(ErrorMessage = "PostedOn: Field is required")]
        public DateTime PostedOn
        { get; set; }

        public DateTime? Modified
        { get; set; }

        public virtual ICollection<Tag> Tags
        { get; set; }

         [InverseProperty("Post")]
        public virtual ICollection<Comment> Comments
        { get; set; }

         [InverseProperty("Post")]
         public virtual ICollection<PostLike> PostLikes
         { get; set; }

         public int PostLikesCount
         { get; set; }

         public int PostDislikesCount
         { get; set; }
        
        public int CommentsCount        
        { get; set; }
        
        [NotMapped]
        public int PrevPostId
        { get; set; }

         [NotMapped]
        public int NextPostId
        { get; set; }

         [NotMapped]
         public int LastPostId
         { get; set; }

         [NotMapped]
         public int FirstPostId
         { get; set; }

         [NotMapped]
         public int PageNumber
         { get; set; }

         [NotMapped]
         public int PostsCount
         { get; set; }
    }
}