﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;



namespace Blog.Models.Blog
{
    public class Tag
    {
        public int Id
        { get; set; }

        [Required(ErrorMessage = "Tag Name: Field is required")]
        [StringLength(500, ErrorMessage = "Tag Name: Length should not exceed 500 characters")]
        public string Name
        { get; set; }

        [NotMapped]
        public int TagUsedCount { get; set; }


        [JsonIgnore]
        public virtual ICollection<Post> Posts
        { get; set; }
    }

    /*
     var result16 = tags
                            .GroupBy(c => c, c => c, new TagComparer())
                            .OrderByDescending(c => c.Count())
                            .Take(20)
                            .Select(c => new { Toppings = c.Key.toppings, Orders = c.Count() });
     */


    public class TagComparer : IEqualityComparer<IEnumerable<Tag>>
    {

        public bool Equals(IEnumerable<Tag> sampleTags, IEnumerable<Tag> tags)
        {
            if (Object.ReferenceEquals(sampleTags, tags)) return true;
            if (Object.ReferenceEquals(sampleTags, null) || Object.ReferenceEquals(tags, null))
                return false;

            int count = tags.Count();
            if (count == sampleTags.Count() && count == tags.Intersect(sampleTags).Count())
                return true;

            return false;
        }

        public int GetHashCode(IEnumerable<Tag> tags)
        {
            if (Object.ReferenceEquals(tags, null)) return 0;

            int hashcode = tags.Aggregate(0, (result, next) => result ^ next.Id.GetHashCode());

            return hashcode;
        }
    }
}