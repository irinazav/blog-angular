﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.Blog;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Blog.ViewModels
{
    public class AddCommentViewModel
    {
       
        public const int MAXCOMMENTBODY = 3000;

        [Range(1, MAXCOMMENTBODY, ErrorMessage = "Comment  charachers count must be between 1 and 3000")]
        public int CommentCharCount { get; set; }
        public Comment Comment { get; set; }
        
    }
}