﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.Blog;

namespace Blog.ViewModels
{
    public class PostsViewModel
    {
        public IEnumerable<Post> Posts { get; set; }
        public IEnumerable<Tag> Tags { get; set; }
        public int PageNumber { get; set; }
        public int PagesCount { get; set; }
    }
}