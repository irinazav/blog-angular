﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Blog.Models.Blog;
using Blog.Repositories.Blog;
using Blog.ViewModels;

namespace Blog.Controllers
{
    public class BlogApiController : ApiController
    {
        public const int PostsPerPage = 10;
       private IBlogRepository<Post, int> _repo;

       public BlogApiController(IBlogRepository<Post, int> repo)
       {
           _repo = repo;
       }

      /* public BlogApiController()
       {
           _repo = new BlogRepository(new Repositories.DbContextEF());
       }*/

       public PostsViewModel Get()//int id = 1)
        {
            var pageNumber = 1;// id;
            var vm = _repo.GetPosts(pageNumber, PostsPerPage);
            vm.Tags = _repo.GetTags();
            vm.PageNumber = pageNumber;
            return vm;
        }
    }
}
