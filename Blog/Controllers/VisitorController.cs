﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Repositories;
using Blog.Repositories.Visitor;
using Blog.Models.Visitor;
using Blog.Filters;

namespace Blog.Controllers
{
    public class VisitorController : Controller
    {
         private IVisitorRepository<ArxiveVisitor, int> _repo;

       public VisitorController(IVisitorRepository<ArxiveVisitor, int> repo)
       {
           _repo = repo;
       }

       public static void JobSaveVisitorActionLog(VisitorActionLog log)
       {
           var db = new DbContextEF();
           IVisitorRepository<ArxiveVisitor, int> repository = new VisitorRepository(db);
           var e = repository.SaveVisitorActionLog(log);
       }
        //==================================
       public ActionResult WhoIsOnline()
       {

           var visitors = OnlineVisitorsContainer.Visitors == null || OnlineVisitorsContainer.Visitors.Count() == 0 ?
                        0 : OnlineVisitorsContainer.Visitors.Count();

           return View(visitors);

       }
       //==================================================
       //[AcceptVerbs(HttpVerbs.Post)]
       [VisitorActionFilter(ActionType = (int)ActionType.Report)]
       public JsonResult GetOnlineVisitorsMap()
       {
           if (OnlineVisitorsContainer.Visitors != null)
           {

               var visitors = OnlineVisitorsContainer.Visitors;



               visitors.Values.Where(v => v.AuthUser == null || v.AuthUser == "").Count();
               var users = visitors.Values.Where(v => v.AuthUser != null && v.AuthUser != "")
                            .GroupBy(v => v.AuthUser, v => v)
                            .Select(h => new
                            {
                                username = h.Key,
                                city = h.FirstOrDefault().GeoLocation.city,
                                ip = h.FirstOrDefault().GeoLocation.ip,
                                country_name = h.FirstOrDefault().GeoLocation.country_name,
                                code = h.FirstOrDefault().GeoLocation.country_code,
                                lat = h.FirstOrDefault().GeoLocation.latitude,
                                lon = h.FirstOrDefault().GeoLocation.longitude
                            });

               var ananimouses = visitors.Values.Where(v => v.AuthUser == null || v.AuthUser == "")
                                 .Select(h => new
                                 {
                                     username = "Anonymous",
                                     city = h.GeoLocation.city,
                                     ip = h.GeoLocation.ip,
                                     country_name = h.GeoLocation.country_name,
                                     code = h.GeoLocation.country_code,
                                     lat = h.GeoLocation.latitude,
                                     lon = h.GeoLocation.longitude
                                 });

               var result = users.Union(ananimouses)
                             .OrderBy(h => h.country_name)
                             .ThenBy(h => h.city)
                             .ThenBy(h => h.username);

               var data = result.Select((d, index) => new
               {
                   name = index,
                   lat = d.lat,
                   lon = d.lon
               }).ToArray();

               var info = result.Select((d, index) => new
               {
                   index = index,
                   ip = d.ip,
                   city = d.city,
                   country_name = d.country_name,
                   code = d.code,
                   username = d.username
               }).ToArray();

               return Json(new
               {
                   data = data,
                   info = info,
                   usersCount = users.Count(),
                   ananimousesCount = ananimouses.Count()

               }

                   , JsonRequestBehavior.AllowGet);
           }

           else
               return Json(new { data = new { } }, JsonRequestBehavior.AllowGet);
       }

       //====================================================

       public ActionResult WhoIsOnlineByCountries()
       {

           var visitors = OnlineVisitorsContainer.Visitors == null ||
                            OnlineVisitorsContainer.Visitors.Count() == 0 ? 0 :
                               OnlineVisitorsContainer.Visitors.Count();

           return View(visitors);

       }
       //======================================================= 
       [AcceptVerbs(HttpVerbs.Get)]
       [VisitorActionFilter(ActionType = (int)ActionType.Report)]
       public JsonResult GetOnlineVisitorsByCountriesMap()
       {
           if (OnlineVisitorsContainer.Visitors != null)
           {

               var visitors = OnlineVisitorsContainer.Visitors;

               var users = visitors.Values
                            .Select(v => new
                            {
                                AuthUser = (v.AuthUser != null && v.AuthUser != "") ? v.AuthUser : v.Anonymous,
                                IsUser = (v.AuthUser != null && v.AuthUser != ""),
                                country_code = v.GeoLocation.country_code,
                                country_name = v.GeoLocation.country_name
                            })
                           .GroupBy(k => k.country_code, k => k)
                           .Select(h => new
                           {
                               code = h.Key,
                               name = h.FirstOrDefault().country_name,
                               usercount = h.Where(r => r.IsUser)
                                     .GroupBy(u => u.AuthUser, u => u.AuthUser).Select(k => k.Key).Count(),
                               ananimouscount = h.Where(r => !r.IsUser)
                                     .GroupBy(u => u.AuthUser, u => u.AuthUser).Select(k => k.Key).Count(),
                           });

               var result = users.Select(c => new
               {
                   code = c.code,
                   name = c.name,
                   usercount = c.usercount,
                   ananimouscount = c.ananimouscount,
                   value = c.usercount + c.ananimouscount
               }).ToArray();


               return Json(new { data = result }, JsonRequestBehavior.AllowGet);

           }
           return Json(new { data = new { } }, JsonRequestBehavior.AllowGet);
       }
        //=====================================================
       public ActionResult GetVisitsStatistics()
       {
           var date = _repo.GetHistoryVisitorsStartDate();
           return View(date);

       }
       //=====================================================
      
       [VisitorActionFilter(ActionType = (int)ActionType.Report)]
       public JsonResult GetVisitorsForMonthChart(int month = 0, int year = 0)
       {

           var date = DateTime.Now;
           month = month == 0 || month > 12 ? date.Month : month;
           year = year == 0 ? date.Year : year;

           var date1 = new DateTime(year, month, 1, 0, 0, 0);
           var date2 = date1.AddMonths(1).AddMilliseconds(-1);

           var visits = _repo.GetVisitsForMonth(date1, date2);
           var days = Enumerable.Range(1, DateTime.DaysInMonth(year, month)).ToList();

           var result = days.GroupJoin(visits,
                                        d => d, v => v.Day,
                                        (d, v) => new
                                        {
                                            Day = d,
                                            Count1 = v.Count() == 0 ? 0 : v.FirstOrDefault().Count1,
                                            Count2 = v.Count() == 0 ? 0 : v.FirstOrDefault().Count2,
                                            Count3 = v.Count() == 0 ? 0 : v.FirstOrDefault().Count3,
                                        }).OrderBy(d => d.Day).ToList();


           return Json(new
           {
               data = new
               {
                   summary = result.Where(r => r.Count1 > 0).ToList(),
                   visits = result.Select(v => v.Count1).ToArray(),
                   users = result.Select(v => v.Count2).ToArray(),
                   ananimuses = result.Select(v => v.Count3).ToArray(),
                   visitors = result.Select(v => v.Count3 + v.Count2).ToArray(),
                   days = days.ToArray(),
                   month = month,
                   year = year,
               }
           }, JsonRequestBehavior.AllowGet);
       }


       //====================================================
       public ActionResult GetHistoryVisitorsByCountry()
       {
           var date = _repo.GetHistoryVisitorsStartDate();
           return View(date);
       }
       //======================================================= 
       [AcceptVerbs(HttpVerbs.Get)]
       [VisitorActionFilter(ActionType = (int)ActionType.Report)]
       public JsonResult GetHistoryVisitorsMap(int month = 0, int year = 0)
       {

           var date = DateTime.Now;
           month = month == 0 || month > 12 ? date.Month : month;
           year = year == 0 ? date.Year : year;

           var date1 = new DateTime(year, month, 1, 0, 0, 0);
           var date2 = date1.AddMonths(1).AddMilliseconds(-1);


           var visitors = _repo.GetVisitsForMonthByCountry(date1, date2);

           var result = visitors.Select(c => new
           {
               code = c.Id2,//country code
               name = c.Title, // country name
               value = c.Count1, //visits
               visitors = c.Count2 + c.Count3,
               usercount = c.Count2,
               ananimouscount = c.Count3,             

           }).ToArray();


           return Json(
               new { data = new { result = result, month = month, year = year } 
               }, JsonRequestBehavior.AllowGet
           );
       }

        //======================================================= 

       [VisitorActionFilter(ActionType = (int)ActionType.Report)]
       public ActionResult GetVisitorStatistics()
       {
           
           var date = DateTime.Now;

           return View();
       }

       [VisitorActionFilter(ActionType = (int)ActionType.Report)]
       public ActionResult GetVisitedPages()
       {
           var actions = _repo.GetVisitorActions();
           if (Request.IsAjaxRequest())
            {
                return Json( 
                    actions.Select(r => new {
                         Action = r.Action,
                         SpentTime = TimeSpan.FromTicks(r.SpentTimeMC).ToString(),
                         Title1 = r.Title1,
                         Title2 = r.Title2,
                         Id1 = r.Id1,
                         Id2 = r.Id2,
                         Count1 = r.Count1,
                         Count2 = r.Count2,
                         Count3 = r.Count3
                    }).ToList(),
                                       
                    JsonRequestBehavior.AllowGet);
            }
            else
                   return PartialView(actions);
       }

        
        //=======================================================
        [VisitorActionFilter(ActionType = (int)ActionType.Report)]
       public ActionResult GetAllVisitorsByIp()
       {
           var actions = _repo.GetAllVisitorsByIp();
           if (Request.IsAjaxRequest()) {

               var visitors = actions.GroupBy(v => v.Country.Name)
                               .Select(h => new
                               {
                                   CountryName = h.Key,

                                   CountryVisitors = h.GroupBy(hh => hh.IpAddress)
                                   .Select(hhh => new
                                   {
                                       IpAddress = hhh.Key,
                                       Address = hhh.FirstOrDefault(),
                                       Visitors = hhh.GroupBy(g => g.UserId)
                                       .Select(gg => new
                                       {
                                           UserName = gg.Key == null ? null : gg.FirstOrDefault().User.UserName,
                                           DateTimes = gg.Select(ggg => ggg.SessionStarted.ToString("MM/dd/yy H:mm:ss"))
                                       })
                                   })
                               }).ToList();

               return Json(visitors, JsonRequestBehavior.AllowGet);
           }
           else
           return View(actions);
       }
    }
}