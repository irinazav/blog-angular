﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Blog.Repositories;
using Blog.Repositories.Admin;
using Blog.Models.Blog;
using Blog.ViewModels;

namespace Blog.Controllers
{
    public class AdminController : Controller
    {
        public const int PageSize = 5;
       private IAdminRepository<Post, int> _repo;

       public AdminController(IAdminRepository<Post, int> repo)
       {
           _repo = repo;
       }

        //Blog pages-------------------------
       public ActionResult GetBlogPages()
       {
           var pages = _repo.GetBlogPagesShort();
           

           if (Request.IsAjaxRequest())
           {
               return Json(
                     pages, JsonRequestBehavior.AllowGet);
           }
           else return View(pages);
       }

       public ActionResult GetBlogPage(int id)
       {
           var page = _repo.GetBlogPage(id);


           if (Request.IsAjaxRequest())
           {
               return Json(
                     page, JsonRequestBehavior.AllowGet);
           }
           else return View(page);
       }
      [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult SaveBlogPage(BlogPage blogPage)
       {
           var id = _repo.UpdateBlogPage(blogPage);


           if (Request.IsAjaxRequest())
           {
               return Json(
                     id, JsonRequestBehavior.AllowGet);
           }
           else return View(id);
       }

        // POSTS ------------------------------------
       public ActionResult GetPosts(int pageNo = 1, int pageSize = PageSize)
       {
           var posts = _repo.GetPostsShort(pageNo, pageSize);
           var count = _repo.GetPostsCount();

           if (Request.IsAjaxRequest()) {
               return Json(
                      new
                      {
                          pageNo = pageNo,
                          pageSize = pageSize,
                          count = count,
                          posts = posts
                      }, JsonRequestBehavior.AllowGet);
           }
           else return View(posts);
       }

      
       public ActionResult GetPostsShortAjax(int pageNo = 1, int pageSize = PageSize)
       {
           var posts = _repo.GetPostsShort(pageNo, pageSize);
           var count = _repo.GetPostsCount();
           return Json(
           new
           {
               pageNo = pageNo,
               pageSize = pageSize,
               count = count,

               posts = posts
           },
           JsonRequestBehavior.AllowGet);
       }

       [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult DeletePost(int id)
       {
           var result = _repo.DeletePost(id);
           return Json(
           new
           {
               result = result
           },
           JsonRequestBehavior.AllowGet);
       }


       [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult InsertOrUpdatePostAjax(Post post)
       {
           var postSaved = _repo.InsertOrUpdatePost(post);
           return Json(
           new
           {
               post = new { Id = postSaved.Id, Title = postSaved.Title }
           },
           JsonRequestBehavior.AllowGet);
       }
       [HttpPost]
       [ValidateAntiForgeryToken]
       [ValidateInput(false)]
       public ActionResult InsertOrUpdatePost(Post post)
       {
           var postSaved = _repo.InsertOrUpdatePost(post);
           return RedirectToAction("GetPosts");
       }
        
       
      
       
       public ActionResult EditPostAjax(int id = 0)
       {
           var post = id == 0 ? new Post() { Tags = new List<Tag>() } : _repo.GetPost(id);
           var tags = _repo.GetAllTags();

           var postTags = (id > 0) ?

              tags.GroupJoin(
                 post.Tags,
                 t1 => t1.Id, t2 => t2.Id,
                 (t1, t2) => new
                 {
                     CheckedValue = t1.Id,
                     Name = t1.Name,
                     Id = t2.FirstOrDefault() != null ? t1.Id : 0
                 }
               )
               .OrderBy(t => t.Name)
               .ToList() :

               tags.Select(t => new { 
                     CheckedValue = t.Id,
                     Name = t.Name,
                     Id = 0
                 }).ToList();

           return Json(
              new
              {
                  post = new
                  {
                      Id = post.Id,
                      Title = post.Title,
                      Description = post.Description,
                      Body = post.Body,
                      Published = post.Published,
                      PostedOn = post.PostedOn,
                      Tags = postTags.OrderBy(t => t.Name)
                  }
              },
           JsonRequestBehavior.AllowGet);
       }

       [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult GetTagsForNewPost()
       {
          
           var tags = _repo.GetAllTags().Select(t => new 
                 {
                     Id = t.Id,
                     Name = t.Name,
                     Checked = false
                 }
               )
               .OrderBy(t => t.Name)
               .ToList();

           return Json(
              new
              {
                  Tags = tags
              },
           JsonRequestBehavior.AllowGet);
       }


       public ActionResult EditPost(int id = 0)
       {
           var tags = _repo.GetAllTags();
           var post = id == 0 ? new Post() { Tags = new List<Tag>() } : _repo.GetPost(id);
           

           var postTags = tags.GroupJoin(
                 post.Tags,
                 t1 => t1.Id, t2 => t2.Id,
                 (t1, t2) => new PostTagViewModel
                 {
                     Id = t1.Id,
                     Name = t1.Name,
                     Checked = (t2.FirstOrDefault() != null )
                 }
               )
               .OrderBy(t => t.Name)
               .ToList();

          
            var vm =   new EditPostViewModel
              {
                 
                      PostTagsCount = post.Tags.Count(),
                      Post = post,
                      Tags = postTags
                 
              };
            return View(vm);
       }

      

        // TAGS --------------------------------------------------
       public ActionResult GetTags(int pageNo = 1, int pageSize = PageSize)
       {
           var tags = _repo.GetTags(pageNo, pageSize);
           var count = _repo.GetTagsCount();
           if (Request.IsAjaxRequest())
           {
               return Json(
                             new
                             {
                                 pageNo= pageNo,
                                 pageSize = pageSize,
                                 count = count,
                                 tags = tags.Select(tag => new
                                 {
                                     Id = tag.Id,
                                     Name = tag.Name
                                 })
                             },JsonRequestBehavior.AllowGet);
           }
           else return View(tags);
       }



     
       public ActionResult GetTagsAjax(int pageNo = 1, int pageSize = PageSize)
       {  var tags = _repo.GetTags(pageNo, pageSize);
            return Json(
            new
            {
                tags = tags.Select(tag => new {
                    Id= tag.Id, 
                    Name= tag.Name })                  
            },
            JsonRequestBehavior.AllowGet);
        }

       [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult DeleteTag(int id)
       {
           var result = _repo.DeleteTag(id);
           return Json(
           new
           {
               result = 8//result
           },
           JsonRequestBehavior.AllowGet);
       }


      [AcceptVerbs(HttpVerbs.Post)]
       public ActionResult InsertOrUpdateTag(Tag tag)
       {
           var tagSaved = _repo.InsertOrUpdateTag(tag);
           return Json(
           new
           {
               tag = new { Id = tagSaved.Id, Name = tagSaved.Name }
           },
           JsonRequestBehavior.AllowGet);
       }

      // need to rename
       public ActionResult EditTag(int id)
       {
           var tag = _repo.GetTag(id);
           
            return Json(
               new
           {
               tag = new {Id = tag.Id, Name = tag.Name}
           },
            JsonRequestBehavior.AllowGet);       
       }

       public ActionResult GetTag(int id)
       {
           var tag = _repo.GetTag(id);

           return Json(
              new
              {
                  tag = new { Id = tag.Id, Name = tag.Name }
              },
           JsonRequestBehavior.AllowGet);
       }   
    }
}