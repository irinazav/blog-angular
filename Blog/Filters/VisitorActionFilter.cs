﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using System.Web.Mvc;
using Blog.Models.Visitor;
using Hangfire;
using Hangfire.Dashboard;
using Blog.Controllers;

namespace Blog.Filters
{
    public class VisitorActionFilter: ActionFilterAttribute, IActionFilter
    {
        public string UserName = "";
        public string PostId = "";
        public string TagId = "";
        public int ActionType = 0;
        void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
        {

            int postId = 0;
            if (!String.IsNullOrEmpty(PostId) && filterContext.ActionParameters.ContainsKey(PostId))
            {
                int? id = filterContext.ActionParameters[PostId] as Int32?;
                postId = id.GetValueOrDefault();
            }

            int tagId = 0;
            if (!String.IsNullOrEmpty(TagId) && filterContext.ActionParameters.ContainsKey(TagId))
            {
                int? id = filterContext.ActionParameters[TagId] as Int32?;
                tagId = id.GetValueOrDefault();
            }



            

            VisitorActionLog log = new VisitorActionLog()
            {
                Controller = filterContext.ActionDescriptor.ControllerDescriptor.ControllerName,
                Action = filterContext.ActionDescriptor.ActionName,
                IpAddress = filterContext.HttpContext.Request.UserHostAddress,
                DateTime = filterContext.HttpContext.Timestamp,
                VisitorUserName = filterContext.HttpContext.User.Identity.Name,
                Anonymous = filterContext.HttpContext.Request.AnonymousID,
                ActionType = ActionType,
                PostId = postId,
                TagId = tagId,
                SessionId = filterContext.HttpContext.Session.SessionID
            };

            BackgroundJob.Enqueue(() => VisitorController.JobSaveVisitorActionLog(log));
            base.OnActionExecuting(filterContext);
        }

    }
}