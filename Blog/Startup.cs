﻿using Microsoft.Owin;
using Owin;
using Hangfire;
using Hangfire.Dashboard;
using Blog.Repositories;
using Blog.Controllers;
using Blog.Filters;

[assembly: OwinStartupAttribute(typeof(Blog.Startup))]
namespace Blog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            // db must be created
            using (var context = new DbContextEF())
            {

                context.Database.Initialize(false);
            }
            //set up jobs
            SetJobs(app);
        }

        private void SetJobs(IAppBuilder app)
        {
            GlobalConfiguration.Configuration.UseSqlServerStorage("DefaultConnection");

            app.UseHangfireDashboard("/Hangfire", new DashboardOptions()
            {
                Authorization = new[] { new HangFireAuthorizationFilter() }
            });


            // app.UseHangfireDashboard();
            app.UseHangfireServer();

           // RecurringJob.AddOrUpdate(() => VisitorController.JobRatings(), VisitorController.CRON_STRING_Rating);
           // RecurringJob.AddOrUpdate(() => VisitorController.JobAddVisitorLogsToArxives(), VisitorController.CRON_STRING_VisitorLogsToArxives);

        }
    }
}
