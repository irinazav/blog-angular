﻿
var app = angular.module('blog', ['ui.router', 'ui.bootstrap', 'ngSanitize', 'ui.tinymce']);


app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
}]);



app.config(function ($stateProvider, $urlRouterProvider) {
	
    $urlRouterProvider.otherwise('/');

    $stateProvider
	.state('app',{
	    url: '/',
	    views: {
	        'header': {
	            templateUrl: '/blogAngular/templates/partials/header.html'
	        },
	        'content': {
	            templateUrl: '/blogAngular/templates/partials/content.html'
	        },	        
	        'footer': {
	            templateUrl: '/blogAngular/templates/partials/footer.html'
	        }
	    }
	})

        .state('app.blog', {
            url: '/blog',

            resolve: {
               
                tagsInfo: function (blogResource) {
                    return blogResource.getTags();
                }
            },

            views: {
                'content@': {
                    templateUrl: '/blogAngular/templates/partials/contentWith2Sides.html',
                    controller: 'blogMainCtrl',
                }
            }

        })

        .state('app.blog.posts', {
            url: '^/posts',
	   
            resolve: {
                postsInfo: function (blogResource) {                 
                    return blogResource.getPosts();
                }               
            },
            
            views: {
                'mainContent': {
                    templateUrl: '/blogAngular/templates/blog/posts.html',	           
                    controller: 'blogPostsCtrl',
                },
            }
       
        })

         .state('app.blog.postsByPage', {
             url: '^/posts/page/:pageNo',

             resolve: {
                 postsInfo: function (blogResource, $stateParams) {
                     return blogResource.getPosts($stateParams.pageNo);
                 }
             },

             views: {
                 'mainContent': {
                     templateUrl: '/blogAngular/templates/blog/posts.html',
                     controller: 'blogPostsCtrl',
                 },
             }

         })
        .state('app.blog.postsByTag', {

            url: '^/postsByTag/:tagId',
            resolve: {
                postsInfo: function (blogResource, $stateParams) {
                    return blogResource.getPostsByTag($stateParams.tagId);
                },
            },
            views: {
                'mainContent': {
                    templateUrl: '/blogAngular/templates/blog/posts.html',
                    controller: 'blogPostsCtrl',
                },               
            }
        })

        .state('app.blog.postsByTagAndPage', {

            url: '^/postsByTag/:tagId/page:pageNo',
            resolve: {
                postsInfo: function (blogResource, $stateParams) {
                    return blogResource.getPostsByTag($stateParams.tagId, $stateParams.pageNo);
                },
            },
            views: {
                'mainContent': {
                    templateUrl: '/blogAngular/templates/blog/posts.html',
                    controller: 'blogPostsCtrl',
                },
            }
        })
        
      .state('app.blog.post', {

             url: '^/post/:id',
             resolve: {
                 postInfo: function (blogResource, $stateParams) {
                     return blogResource.getPost($stateParams.id);
                 },
             },
             views: {
                 'mainContent': {
                     templateUrl: '/blogAngular/templates/blog/post.html',
                     controller: 'blogPostCtrl',
                 },
             }
      })

        .state('app.blog.blogPages', {
            url: '^/:description',
            resolve: {
                pageInfo: function (blogResource, $stateParams) {
                    return blogResource.getBlogPage($stateParams.description);
                },
            },
            views: {
                'mainContent': {
                    template: '<div ng-bind-html="content"></div>',
                    controller: 'blogBlogPageCtrl',
                },
            }

        }) 

        

	.state('app.adminTags', {
	    url: '^/manageTags',
	   
	    resolve: {
	        tagsInfo: function (adminResource) {
	            return adminResource.getTags();
	        }
	    },
	   
	    views: {
	        'content@': {
	            templateUrl: '/blogAngular/templates/admin/listTags.html',	           
	            controller: 'adminTagsCtrl',
	        },
	    }
		
	})

   .state('app.adminTagsByPage', {
       url: '^/manageTags/Page/:pageNo',
       resolve: {
           tagsInfo: function (adminResource, $stateParams) {
               return adminResource.getTags($stateParams.pageNo);
           }
       },
       views: {
           'content@': {
               templateUrl: "/blogAngular/templates/admin/listTags.html",
               controller: 'adminTagsCtrl'
           }
       }
   })

    .state('app.adminTags.grid', {
        url: '/Page/:pageNo',
        views: {
            'grid@blog.adminTags': {
                templateUrl: '/blogAngular/templates/admin/tagsGrid.html',
               // controller: 'adminTagsGridCtrl',
            }
        }

    })
	


	.state('app.adminPosts', {
	    url: '^/managePosts',
	    resolve: {
	        postsInfo: function (adminResource) {
	            return adminResource.getPosts();
	        }
	    },
	    views: {
	        'content@': {
                
	            templateUrl: "/blogAngular/templates/admin/listPosts.html",
	            controller: 'adminPostsCtrl'
	        }
	    }
	})
    .state('app.adminPostsByPage', {
        url: '^/managePosts/Page/:pageNo',
        resolve: {
            postsInfo: function (adminResource, $stateParams) {
                return adminResource.getPosts($stateParams.pageNo);
            }
        },
        views: {
            'content@': {
                templateUrl: "/blogAngular/templates/admin/listPosts.html",
                controller: 'adminPostsCtrl'
            }
        }
    })

    

    .state('app.adminAddPost', {
            url: '/addPost',
            resolve: {
                postInfo: function (adminResource) {
                    return adminResource.getPost(0);
                }
            },
            views: {
                'content@': {
                    templateUrl: "/blogAngular/templates/admin/editorPost.html",
                    controller: 'adminPostCtrl'
                }
            }
		
    })

    .state('app.adminEditPost', {
        url: '/editPost/:id',
        resolve: {
            postInfo: function (adminResource, $stateParams) {
                return adminResource.getPost($stateParams.id);
            }
        },
        views: {
            'content@': {
                templateUrl: "/blogAngular/templates/admin/editorPost.html",
                controller: 'adminPostCtrl'
            }
        }

    })

    .state('app.adminEditBlogPage', {
        url: '^/editBlogPage/:id',
        resolve: {
            pageInfo: function (adminResource, $stateParams) {
                return adminResource.getBlogPage($stateParams.id);
            }
        },
        views: {
            'content@': {
                templateUrl: "/blogAngular/templates/admin/editorBlogPage.html",
                controller: 'adminBlogPageCtrl'
            }
        }

    })


    .state('app.adminBlogPages', {
        url: '^/listBlogPages',
        resolve: {
            pagesInfo: function (adminResource) {
                return adminResource.getBlogPages();
            }
        },
        views: {
            'content@': {
                templateUrl: "/blogAngular/templates/admin/listBlogPages.html",
                controller: 'adminBlogPagesCtrl'
            }
        }
    })

   
        
     .state('app.adminVisitedPages', {
         url: '^/report/VisitedPages',

         resolve: {

             reportInfo: function (adminResource) {
                 return adminResource.getVisitedPages();
             }
         },

         views: {
             'content@': {
                 templateUrl: '/blogAngular/templates/admin/visitedPages45.html',
                 controller: 'adminVisitedPagesCtrl',
             }
         }

     })

    .state('app.adminVisitorsByIP', {
        url: '^/report/VisitorsByIP',

        resolve: {
            reportInfo: function (adminResource) {
                return adminResource.getVisitorsByIp();
            }
        },

        views: {
            'content@': {
                templateUrl: '/blogAngular/templates/admin/visitotsByIP.html',
                controller: 'adminVisitorsByIPCtrl',
            },
            'footer@': {
                template: '',

            }
        }

    })


 .state('app.onlineVisitors', {
     url: '/onlineVisitors',

     resolve: {

         visitorsInfo: function (visitorResource) {
             return visitorResource.getOnlineVisitorsByCountriesMap();
         },
         title: function () { return "Online visitors by country"; }
   
     },

     views: {
         'content@': {
             templateUrl: '/blogAngular/templates/visitor/onlineVisitorsByCountry.html',
             controller: 'visitorCtrl',
         },
         'footer@': {
             template: '',

         }
     }

 })

.state('app.onlineVisitorsLocatins', {
    url: '/onlineVisitorLocatins',

    resolve: {

        visitorsInfo: function (visitorResource) {
            return visitorResource.getOnlineVisitorsMap();
        },
        title: function () { return "Online visitors by username"; }

    },

    views: {
        'content@': {
            templateUrl: '/blogAngular/templates/visitor/onlineVisitorsByUser.html',
            controller: 'visitorCtrl',
        },
        'footer@': {
            template: '',

        }
    }

})

    

    .state('app.historyVisitors', {
        url: '/historyVisitors',
        params: {
            year: null, month: null,
        },

        resolve: {

            visitorsInfo: function (visitorResource, $stateParams) {
                return visitorResource.getHistoryVisitorsMap($stateParams.month, $stateParams.year);
            },
            title: function () { return "History visitors by country"; }

        },

        views: {
            'content@': {
                templateUrl: '/blogAngular/templates/visitor/historyVisitors.html',
                controller: 'visitorCtrl',
            },
            'footer@': {
                template: '',

            }
        }

    })


    .state('app.historyVisitorsDaily', {
        url: '/app.historyVisitorsByDay',
        params: {
            year: null, month: null,
        },

        resolve: {

            visitorsInfo: function (visitorResource, $stateParams) {
                return visitorResource.getHistoryVisitorsMonthChart($stateParams.month, $stateParams.year);
            },
            title: function () { return "History visitors by day"; }

        },

        views: {
            'content@': {
                templateUrl: '/blogAngular/templates/visitor/historyVisitorsByDay.html',
                controller: 'visitorCtrl',
            },
            'footer@': {
                template: '',
               
            }
        }

    })
         
});




   


