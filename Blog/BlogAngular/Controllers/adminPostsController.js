﻿angular.module('blog')
    .controller("adminPostsCtrl", function ($scope, $state, $location,
        adminResource, userSettings, dialogConfirm, postsInfo, $uibModal) {
                  
            $scope.placement = {
                options: userSettings.optionsAdminGrid,
                selected: userSettings.pageSizeAdminPosts
            };

            $scope.orderBy = "Title";

            $scope.orderByMe = function (x) {
                $scope.orderBy = x;
            }

            $scope.posts = postsInfo.data.posts;
            $scope.totalItems = postsInfo.data.count;
            $scope.itemsPerPage = userSettings.pageSizeAdminPosts;
            $scope.currentPage = postsInfo.data.pageNo;
            $scope.directionLinks = false;
       
            var $ctrl = this;
            $scope.itemsPerPageChanged = function () {
                userSettings.pageSizeAdminPosts = $scope.placement.selected;
                $state.go('app.adminPostsByPage', { pageNo: 1 }, { reload: 'app.adminPostsByPage' });
            }
 
            $scope.pageChanged = function () {
                $state.go('app.adminPostsByPage', { pageNo: $scope.currentPage });
            }

            $scope.refresh = function () {
                $state.go('app.adminPostsByPage', { pageNo: 1 }, { reload: 'app.adminPostsByPage' });
            }
              
            $scope.deletePost = function (id) {
                dialogConfirm("Do you want to delete this post? ", "Confirmation")
                    .then(function (isConfirmDelete) {
                        if (isConfirmDelete.result) {
                            adminResource.deletePost(id)
                             .then(function (response) {
                                 if (response.data.result > 0) {
                                     var index = $scope.posts.map(function (e) { return e.Id; }).indexOf(id);
                                     index != -1 ? $scope.posts.splice(index, 1) : alert("not able to delete");
                                 }
                                 else {
                                     alert("not table to delete")
                                 }
                             });
                        }
                    }, function (r) { });
            }

    })





    