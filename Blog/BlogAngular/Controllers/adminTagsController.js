﻿angular.module('blog')
    .controller("adminTagsCtrl", function ($scope, $http, $state, $location, $uibModal,
                                            adminResource, userSettings, tagsInfo, dialogConfirm) {
    
        var $ctrl = this;
        $ctrl.animationsEnabled = true;

        $scope.orderBy = "Name";
        
        $scope.orderByMe = function (x) {
            $scope.orderBy = x;
        }

        $scope.placement = {
            options: userSettings.optionsAdminGrid,
            selected: userSettings.pageSizeAdminTags
        };

        $scope.tags = tagsInfo.data.tags;
        $scope.totalItems = tagsInfo.data.count;
        $scope.itemsPerPage = userSettings.pageSizeAdminTags;
        $scope.currentPage = tagsInfo.data.pageNo;
        $scope.directionLinks = false;

        $scope.refresh = function () {
            $state.go('app.adminTagsByPage', { pageNo: 1 }, { reload: 'app.adminTagsByPage' });
        }

        $scope.itemsPerPageChanged = function () {
            userSettings.pageSizeAdminTags = $scope.placement.selected;
            $state.go('app.adminTagsByPage', { pageNo: 1 }, { reload: 'app.adminTagsByPage' });
        }


        $scope.pageChanged = function () {
            $state.go('app.adminTagsByPage', { pageNo: $scope.currentPage });
        }
 
         
       

        $scope.deleteTag = function (id) {
            dialogConfirm("Do you want to delete this tag? ", "Confirmation")
                .then(function (isConfirmDelete) {
                    if (isConfirmDelete.result) {
                        adminResource.deleteTag(id)
                         .then(function (response) {
                             if (response.data.result > 0) {
                                 var index = $scope.tags.map(function (e) { return e.Id; }).indexOf(id);
                                 index != -1 ? $scope.tags.splice(index, 1) : alert("not able to delete");
                             }
                             else {
                                 alert("not table to delete")
                             }
                         });
                    }
                }, function (r) { });
        }



        $scope.openTagModal = function (src, size, parentSelector) {        
            var parentElem = parentSelector ? angular.element($document[0]
                                   .querySelector('.modal-demo ' + parentSelector)) : undefined;
            var modalInstance = $uibModal.open({
                animation: $ctrl.animationsEnabled,
                ariaLabelledBy: 'modal-title',
                ariaDescribedBy: 'modal-body',
                templateUrl: "/blogAngular/templates/admin//editorTagModal.html",
                controller: 'ModalInstanceCtrl',
                controllerAs: '$ctrl',
                size: size,
                appendTo: parentElem,
                resolve: {
                    src: function () {return src;}
                }
            });

            modalInstance.result.then(function (result) {
                if (result.isNew)
                        $scope.tags.push(result.tag);
            }, function () {
               // $log.info('Modal dismissed at: ' + new Date());
            });
        };


    })


angular.module('blog').controller('ModalInstanceCtrl', function ($uibModalInstance, adminResource, src) {

        var $ctrl = this;
        $ctrl.currentTag = src.tag || {Id: 0, Name: ""};
        $ctrl.currentIndex = src.index || -1;
        $ctrl.tags = src.tags;

        $ctrl.isSaved = true;

        $ctrl.ok = function () {     
            adminResource.insertOrUpdateTag($ctrl.currentTag)
             .then(function (response) {
                 var tagFromDB = response.data.tag;
                 if (tagFromDB.Id > 0) {                                   
                     $uibModalInstance.close({ tag: tagFromDB, isNew: $ctrl.currentTag.Id == 0 });
                 }
                 else {
                     $ctrl.isSaved = false;
                 }
             });      
        };


        $ctrl.cancel = function () {
            if ($ctrl.currentTag.Id > 0) {
                adminResource.getTag($ctrl.currentTag.Id).then(function (result) {
                    $ctrl.currentTag.Name = result.data.tag.Name;
                });
            }             
            $uibModalInstance.dismiss('cancel');
        };

    });

   
