﻿
angular.module('blog').controller("blogPostsCtrl", function ($scope, $http, $state, 
                   $uibModal, $document, blogResource, postsInfo, userSettings) {

    $scope.tagId = postsInfo.data.TagId || 0;
   
    $scope.data = postsInfo.data;
    $scope.posts = postsInfo.data.Posts;
    $scope.totalItems = postsInfo.data.PagesCount;
    $scope.currentPage = postsInfo.data.CurrentPage;
    $scope.nextText = "Next " + userSettings.pageSizeBlogPosts + " »";
    $scope.prevText = "« " + userSettings.pageSizeBlogPosts + " Previous";

    $scope.placement = {
        options: userSettings.optionsBlogPosts,
        selected: userSettings.pageSizeBlogPosts
    };
    
    $scope.itemsPerPageChanged = function () {
        userSettings.pageSizeBlogPosts = $scope.placement.selected;
        if ($scope.tagId == 0)
            $state.go('app.blog.postsByPage', { pageNo: 1 },
                                              { reload: 'app.blog.postsByPage' });
        else
            $state.go('app.blog.postsByTagAndPage', { pageNo: 1, tagId: $scope.tagId },
                                                    { reload: 'app.blog.postsByTagAndPage' });
    }

    $scope.pageChanged = function () {
        if ($scope.tagId == 0)
            $state.go('app.blog.postsByPage', { pageNo: $scope.currentPage });
        else
            $state.go('app.blog.postsByTagAndPage', { pageNo: $scope.currentPage, tagId: $scope.tagId },
                                                    { reload: 'app.blog.postsByTagAndPage' });
    }

            
$scope.togglePostBody = function (postId, index) {
    if (!$scope.posts[index].Body) {
        blogResource.getExpandPost(postId).then(function (responce) {
            $scope.posts[index].Body = responce.data.Body;
            $scope.posts[index].isPostBodyOpen = true;
        });
    }
    $scope.postId = postId;
    $scope.posts[index].isPostBodyOpen = !$scope.posts[index].isPostBodyOpen;
}

$scope.postBodyCaption = function (index) {
    return ($scope.posts[index].isPostBodyOpen || false) ? "〈 Hide content 〉" : "〈  Read more ... 〉";
}

$scope.getTagTitleMessage = function (tagName) {
    return ("Get posts by tag " + tagName);               
}
            

           

$scope.addPostLike = function (postId, IsAuthenticated, index, isLike) {
               
    if (IsAuthenticated)
    {
                   
        blogResource.addPostLike(postId, isLike).then(function (responce) {

            if (responce.data.result < 0)
            {
                alert("You can't rate twice");
                return;
            }            
            if (isLike) 
                    $scope.posts[index].PostLikesCount = responce.data.result;
            else
                 $scope.posts[index].PostDislikesCount = responce.data.result;                                                 
           });
    }
    else
    {
        alert("Anonymous can't rate");
    }
}
           
});