﻿


angular.module('blog').controller("blogPostCtrl", function ($scope,  $state, blogResource,
          postInfo,  $uibModal, $document) {

    $scope.post = postInfo.data.post;  
    var data = $scope.post;

    $scope.totalItems = data.PostsCount;
    $scope.currentPage = data.PageNumber;
    $scope.oldCurrentPage = data.PageNumber;
    $scope.nextPostId = data.NextPostId;
    $scope.prevPostId = data.PrevPostId;
   
    
    
    $scope.pageChanged = function () {              
        var id = ($scope.oldCurrentPage < $scope.currentPage)?  $scope.nextPostId :$scope.prevPostId;
        $state.go("app.blog.post", { id: id });        
    }
          

    $scope.addPostLike = function (postId, IsAuthenticated,  isLike) {
        if (IsAuthenticated) {

            blogResource.addPostLike(postId, isLike).then(function (responce) {

                if (responce.data.result < 0) {
                    alert("You can't rate twice");
                    return;
                }

                if (isLike)
                    $scope.post.PostLikesCount = responce.data.result;
                else
                    $scope.post.PostDislikesCount = responce.data.result;
            });
        }
        else {
            alert("Anonymous can't rate");
        }
    }
    
           
});