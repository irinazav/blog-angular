﻿angular.module('blog').controller("visitorCtrl", function ($scope,$state,
                   $uibModal, visitorsInfo, title) {
    $scope.visitorsInfo = visitorsInfo;
    $scope.title = title;
   

  $scope.setDate = function (year, month) {
        var dt1 = new Date(year, month, 0);
        var dt2 = new Date();
        $scope.dt = (dt1 > dt2) ? dt2 : dt1;
  };

  $scope.setDate(visitorsInfo.data.data.year, visitorsInfo.data.data.month);
    
    $scope.today = function () {
        $scope.dt = new Date();
    };
   
    $scope.change = function () {
        if ($scope.dt == null) return;
        var month = $scope.dt.getMonth();
        var year = $scope.dt.getFullYear();
        $state.go ($state.current, { month: month +1 , year: year}, { reload: $state.current });       
    }
   
    $scope.clear = function () {
        $scope.dt = null;
    };

    $scope.inlineOptions = {
        customClass: getDayClass,
        minDate: new Date(),
        showWeeks: true
    };

    $scope.dateOptions = {
        formatYear: 'yy',
        startingDay: 1,
        minMode: 'month',
        dateDisabled: disabled,
        formatYear: 'yy',
        maxDate: new  Date(),
        minDate: new Date(2016, 1, 1),
        
    };


    // Disable weekend selection
    function disabled(data) {
        var date = data.date,
          mode = data.mode;
        return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    }

    $scope.toggleMin = function () {
        $scope.inlineOptions.minDate = $scope.inlineOptions.minDate ? null : new Date();
        $scope.dateOptions.minDate = $scope.inlineOptions.minDate;
    };

   // $scope.toggleMin();

    $scope.open = function () {
        $scope.popup.opened = true;
    };

    $scope.formats = ['MMMM yyyy', 'MM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
    $scope.format = $scope.formats[0];
    $scope.altInputFormats = ['M!/d!/yyyy'];

    $scope.popup = {
        opened: false
    };

    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
          date: tomorrow,
          status: 'full'
      },
      {
          date: afterTomorrow,
          status: 'partially'
      }
    ];

    function getDayClass(data) {
        var date = data.date,
          mode = data.mode;
        if (mode === 'day') {
            var dayToCheck = new Date(date).setHours(0, 0, 0, 0);

            for (var i = 0; i < $scope.events.length; i++) {
                var currentDay = new Date($scope.events[i].date).setHours(0, 0, 0, 0);

                if (dayToCheck === currentDay) {
                    return $scope.events[i].status;
                }
            }
        }

        return '';
    }

    //=============================
    
});


angular.module('blog').controller("MapController", function ($scope,
                   $uibModal) {


    //demo highmap
    var countries = {
        europe: 'France, Germany, Russia',
        asia:   'Japan, China'
    },
            defaultSeriesData = {
                allAreas: false,
                name: '',
                countries: '',
                data: [],
                dataLabels: {
                    enabled: true,
                    color: 'white',
                    formatter: function () {
                        if (this.point.value) {
                            return this.point.name;
                        }
                    }
                },
                tooltip: {
                    enabled: true,
                    headerFormat: '',
                    pointFormat: '{point.name}: <b>{series.value} - {series.name}</b>'
                }
            }
    ;

    this.makeSeries = function(name, countries) {
        var seriesData = angular.copy(defaultSeriesData);

        seriesData.name      = name;
        seriesData.countries = countries;
        seriesData.data      = this.makeSeriesData(countries);

        return seriesData;
    };

    this.makeSeriesData = function(string) {
        var list = ('' + string).split(','),
            data = []
        ;

        angular.forEach(list, function(country) {
            data.push({
                name:  country.replace(/^\s+|\s+$/, ''),
                value: 1
            });
        });

        return data;
    };

    this.setSeriesData = function(series, string) {
        series.data = this.makeSeriesData(string);
    };

    this.addSeries = function() {
        this.config.series.push(this.makeSeries());
    };

    this.removeSeries = function(key) {
        this.config.series.splice(key, 1);

        if (1 == this.config.series.length) {
            this.config.series[0].allAreas = true;
        }
    };

    this.config = {
        options: {
            legend: {
                enabled: false
            },
            plotOptions: {
                map: {
                    mapData: Highcharts.maps['custom/world'],
                    
                    joinBy: ['name']
                }
            },
        },
        chartType: 'map',
        title: {
            text: 'Highcharts-ng map example'
        },
        series: [
            this.makeSeries('Europe', countries.europe),
            this.makeSeries('Asia', countries.asia)
        ]
    };

    this.config.series[0].allAreas = true;
   

    
    // online map by city
  this.config2 = { 

        title: {
            text: 'Right now active visitors on site'
        },

        mapNavigation: {
            enabled: true
        },

        tooltip: {
            headerFormat: '',
            // pointFormat: '<b>{point.name}</b><br>Lat: {point.lat}, Lon: {point.lon}'
            formatter: this.fnTooltipFormatter
        },

        series: [{
            // Use the gb-all map with no data as a basemap
            mapData: Highcharts.maps['custom/world-highres'],
            name: 'Basemap',
            borderColor: '#A0A0A0',
            nullColor: 'rgba(200, 200, 200, 0.3)',
            showInLegend: false
        }, {
            name: 'Separators',
            type: 'mapline',
            data: Highcharts.geojson(Highcharts.maps['custom/world-highres'], 'mapline'),
            color: '#707070',
            showInLegend: false,
            enableMouseTracking: false
        }, {
            // Specify points using lat/lon
            type: 'mappoint',
            name: 'visitors',
            // color: Highcharts.getOptions().colors[3],
            "marker": {
                "fillColor": Highcharts.getOptions().colors[1],
                "lineColor": Highcharts.getOptions().colors[1],
                "lineWidth": 1,
                "radius": 2
            },
            dataLabels: {
                enabled: true,
                formatter: this.fnLabelFormatter
                //format: '{point.name}'
            },
            data: $scope.visitorsInfo.data.data
        }]
    };


  this.fnTooltipFormatter =  function () {

        var info = dataInfo[this.point.name];          
        return "<b>Visitor # " + (parseInt(info.index) + 1) + "</b><br/><b>User Name: </b>" +
              info.username + "<br/><b>Location: </b>" +
              info.city + ", " + info.country_name;
            
    }

       
    this.fnLabelFormatter = function () {

        var info = dataInfo[this.point.name];
        return "<b>" +info.city +"</b>";

    }


   

 // online map 
  angular.forEach($scope.visitorsInfo.data.data, function (value, key) {

        value.flag = value.code.replace('UK', 'GB').toLowerCase();
    });

  this.config3=
    {

        title: {
                text: 'Right now website visitors by countries'
        },

        legend: {
            title: {
                    text: 'Visitors',
                    style: {
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                    }
            }
        },

        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'top'
            }
        },

        tooltip: {
            backgroundColor: 'white',
            borderWidth: 2,
            shadow: false,
            useHTML: true,
            padding: 3,
            pointFormat: '<span class="f32"><span class="flag {point.flag}"></span></span> {point.name}<br/>' +                      
                '<b>Total visitors:</b> {point.value}<br/><b>Authenticated:</b> {point.usercount}<br/><b>Anonymous:</b> {point.ananimouscount}<br/>',

            positioner: function () {
                return { x: 0, y: 250 };
            }
        },

        colorAxis: {
            min: 1,
            max: 1000,
            type: 'logarithmic'
        },

        series: [{
            data: $scope.visitorsInfo.data.data,
            mapData: Highcharts.maps['custom/world'],
            joinBy: ['iso-a2', 'code'],
            name: 'Total country visitors',
            states: {
                hover: {
                    color: '#BADA55'
                }
            }
        }]
    };
    //=========================================

    });
       

   
    
      


      
        
