﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Blog
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
               "PostLikes", // Route name
               "Blog/AddPostDislike/{id}", // URL with parameters
               new { controller = "Blog", action = "AddPostLike", id = "id", like = false } 
             );

             routes.MapRoute(
               "GetNames", // Route name
               "Blog/GetNamesWhoDislike/{id}", // URL with parameters
               new { controller = "Blog", action = "GetNamesWhoLike", id = "id", like = false } 
             );

            routes.MapRoute(
                name: "asDefault1",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Blog", action = "GetPosts", id = UrlParameter.Optional }
            );

         /*   routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
           );*/
        }
    }
}
