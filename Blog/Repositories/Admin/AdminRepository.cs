﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Core.Objects;
using System.Reflection;
using Blog.Models.Blog;
using Blog.ViewModels;

namespace Blog.Repositories.Admin
{
    public class AdminRepository: IAdminRepository<Post, int>
    {
        private DbContextEF _context;


        public AdminRepository(DbContextEF context)
        {
            _context = context;
        }


        //Blog pages
        public IEnumerable<BlogPageShortViewModel> GetBlogPagesShort() {
            return _context.BlogPages
                    .Select(k => new BlogPageShortViewModel { Id = k.Id, Description = k.Description });
        }

        public BlogPage GetBlogPage(int id) {
            return _context.BlogPages.Find(id);
        }

        public int UpdateBlogPage(BlogPage page)
        {           
            var originalPage = _context.BlogPages.Find(page.Id);
            if  (originalPage == null) return -1;

            originalPage.Content = page.Content;
            _context.Entry(originalPage).State = EntityState.Modified;
            _context.SaveChanges();
            return originalPage.Id;
        }


        //-POSTS --------------------------

        public Post GetPost(int id)
        {
            var post = _context.Posts.Where(p=> p.Id == id).Include(p => p.Tags).FirstOrDefault();
            post.Tags = post.Tags.OrderBy(t => t.Name).ToList();
            return post;
        }

        public int GetPostsCount()
        {
            var count = _context.Posts.Count();
            return count;
        }

        public IEnumerable<PostShortViewModel> GetPostsShort(int pageNo, int pageSize)
        {
            var posts = _context.Posts                    
                     .OrderBy(p => p.Title)
                     .Skip(pageSize * (pageNo - 1))
                     .Take(pageSize)
                     .Include(p => p.Tags)
                     .Select( p => new  
                     {
                         Id = p.Id, 
                         Title = p.Title,
                         PostedOn = p.PostedOn,
                         Published = p.Published,
                         Tags = p.Tags
                     })
                     .AsEnumerable()
                     .Select(p => new PostShortViewModel()
                     {
                         Id = p.Id,
                         Title = p.Title,
                         PostedOn = p.PostedOn.ToString("dd-MMM-yyyy"),
                         Published = p.Published,
                         Tags = string.Join(", ", 
                                p.Tags.Select(u => u.Name).OrderBy(n => n).ToArray())
                     })
                     .ToList();
            return posts;
        }


        public int DeletePost(int id)
        {
            var post = _context.Posts.Find(id);
            if (post == null) return 0;
            _context.Posts.Remove(post);
            _context.SaveChanges();
            return id;
        }


        public Post InsertOrUpdatePost(Post post)
        {           
            var tagIds = post.Tags.Where(t => t.Id > 0).Select(t => t.Id).ToList();

            if (post.Id == 0) // new post
            {
                var originTags = _context.Tags.Where(t =>tagIds.Any(tt => tt == t.Id)).ToList();
                post.Tags = originTags;
                _context.Posts.Add(post);

            }
            else // old post
            {
                var originalPost = _context.Posts.
                    Where(p => p.Id == post.Id)
                    .Include(p => p.Tags)
                    .FirstOrDefault();


                if (originalPost == null) return new Post();
                else
                { 
                    

                        //Delete
                        var tagsToDelete = originalPost.Tags.GroupJoin(
                            tagIds,
                            i1 => i1.Id, i2 => i2,
                                (i1, i2) => new { OriginalTag = i1, ToDelete = i2.FirstOrDefault() == 0 }
                            )
                            .Where(t => t.ToDelete)
                            .Select(u => u.OriginalTag)
                            .ToList();

                        foreach (var tag in tagsToDelete)
                        {
                            originalPost.Tags.Remove(tag);
                        }
                       

                        //Add
                        var tagIdsToAdd = tagIds
                                .Where(t => originalPost.Tags.All(tt => tt.Id != t))
                                .Select(k => k)
                                .ToList();

                        var tagsToAdd = _context.Tags
                                .Where(t => tagIdsToAdd.Any(tt => t.Id == tt))
                                .ToList();

                        foreach (var tag in tagsToAdd)
                        {
                            originalPost.Tags.Add(tag);
                        }

                        //Update
                        originalPost.Modified = DateTime.Now;
                        originalPost.Published = post.Published;
                        originalPost.Title = post.Title;
                        originalPost.Description = post.Description;
                        originalPost.Body = post.Body;
                        _context.Entry(originalPost).State = EntityState.Modified;
                   
                }
            }
            _context.SaveChanges();
            return post;
        }




        // TAGS ---------------------------
        public Tag GetTag(int id)
        {
            var tag = _context.Tags.Find(id);
                     
            return tag;
        }

        public IEnumerable<Tag> GetAllTags()
        {
            var tags = _context.Tags                     
                     .ToList();
            return tags;
        }
        public IEnumerable<Tag> GetTags(int pageNo, int pageSize)
        { 
           var  tags =_context.Tags
                    .OrderBy(p => p.Name)
                    .Skip(pageSize * (pageNo - 1))
                    .Take(pageSize)                  
                    .ToList();
           return tags;
        }

        public int GetTagsCount()
        {        
            return _context.Tags.Count();
        }

        public int DeleteTag(int id)
        {
            var tag = _context.Tags.Find(id);

            var posts = _context.Tags.Where(t => t.Id == id)
                        .Include(t => t.Posts)
                        .SelectMany(k => k.Posts, (tt, kk) => kk).ToList();
           
                foreach (var p in posts)
                {
                    p.Tags.Remove(tag);
                }
           
            _context.Tags.Remove(tag);
            _context.SaveChanges();
            return id;
        }



        public Tag InsertOrUpdateTag(Tag tag)
        {
            if (tag.Id == 0)
            {
                _context.Tags.Add(tag);
            }
            else
            {
                var original = _context.Tags.Find(tag.Id);
                if (original == null) return new Tag();
                else if (original.Name == tag.Name) return tag;
                else {
                     original.Name = tag.Name;
                    _context.Entry(original).State = EntityState.Modified; }

            }
            _context.SaveChanges();
            return tag;
        }


    }
}