﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Blog.Models.Blog;
using Blog.ViewModels;

namespace Blog.Repositories.Blog
{
    public interface IBlogRepository<T, TId> where T : class
    {
        BlogPage GetBlogPage(string description);

        PostsViewModel GetPosts(int pageNo, int pageSize);
        PostsViewModel GetPostsByTag(int id, int pageNo, int pageSize);
        Post GetPost(int id);
        Post ExpandPost(int id);

        IEnumerable<Tag> GetTags();
        IEnumerable<Tag> GetTagsWithUsedCount();

        Comment AddComment(Comment comment);

        int AddPostLike(int id, string username, bool like);
        IEnumerable<string> GetNamesWhoLike(int id, bool like);

    }
}