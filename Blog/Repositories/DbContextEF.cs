﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Blog.Models.Blog;
using Blog.Models.User;
using Blog.Models.Visitor;
using Blog.Models;


namespace Blog.Repositories
{
    public class DbContextEF : IdentityDbContext<ApplicationUser>
    {
        //Blog
        public DbSet<BlogPage> BlogPages { get; set; }
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<PostLike> PostLikes { get; set; }

        //Visitor
        public DbSet<VisitorActionLog> VisitorActionLogs { get; set; }
        public DbSet<ArxiveVisitorLog> ArxiveVisitorLogs { get; set; }
        public DbSet<WebVisitor> Visitors { get; set; }
        public DbSet<Country> Countries { get; set; }
        


        public DbContextEF(): base("DefaultConnection")
        { 
             this.Configuration.LazyLoadingEnabled = true; 
         }

        public static DbContextEF Create()
        {
            return new DbContextEF();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<ApplicationUser>()
                .ToTable("Users");

            modelBuilder.Entity<Country>()
                .ToTable("Countries");
          
            modelBuilder.Entity<Tag>()
           .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<BlogPage>()
          .Property(e => e.Id)
          .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Comment>()
            .Property(e => e.Id)
            .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<Post>()
           .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<PostLike>()
           .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<ArxiveVisitor>()
           .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            modelBuilder.Entity<VisitorActionLog>()
           .Property(e => e.Id)
           .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

           

            modelBuilder.Entity<Post>()
             .HasMany<Tag>(post => post.Tags)
             .WithMany(tag => tag.Posts)
             .Map(pt =>
             {
                 pt.MapLeftKey("PostRefId");
                 pt.MapRightKey("TagRefId");
                 pt.ToTable("PostVsTag");
             });


            modelBuilder.Entity<WebVisitor>()   //asp identity
               .HasOptional(c => c.User)
               .WithMany()
               .HasForeignKey(p => p.UserId)
               .WillCascadeOnDelete(false);



        }
    }
}