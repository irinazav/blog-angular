﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Blog.Models.Blog;
using Blog.Models;
using Blog.Models.User;



namespace Blog.Repositories
{
    ///need to add at global.asxx
    //Database.SetInitializer<DbContextEFInitializer>(new DbContextEFInitializerInitializer()); 

public class DbContextEFInitializer : DropCreateDatabaseIfModelChanges<DbContextEF>
//public class DbContextEFInitializer : DropCreateDatabaseAlways<DbContextEF>
    {
        protected override void Seed(DbContextEF context)
        {
            InitializeDB(context);
           
            base.Seed(context);
        }

      
        private void InitializeDB(DbContextEF context)
        {

            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var RoleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));


            // Create roles  
            string[] roles = { "Admin" };
            foreach (string role in roles)
            {
                if (!RoleManager.RoleExists(role))
                {
                    var roleresult = RoleManager.Create(new IdentityRole(role));
                }
            }

            ApplicationUser[] users = { 
                               new ApplicationUser() { UserName = "user1",  Email="test1@yahoo.com" , Created = DateTime.Now.AddDays(-14)}, 
                               new ApplicationUser() { UserName = "user2",  Email="test2@yahoo.com" , Created = DateTime.Now.AddDays(-14)}, 
                               new ApplicationUser() { UserName = "user3",  Email="test3@yahoo.com" , Created = DateTime.Now.AddDays(-14)}, 

                          };

            for (int i = 0; i < users.Length; i++)
            {
                // password = username +"P"
                var identityResult = UserManager.Create(users[i], "test" + (i+1) + "P=");

                if (identityResult.Succeeded)
                {
                    if (users[i].UserName == "user1")
                    {
                        UserManager.AddToRole(users[i].Id, "Admin");
                    }
                }
            }



            Tag tag1 = new Tag { Name = "Нью-Йорк" };
            Tag tag2 = new Tag { Name = "История" };
            Tag tag3 = new Tag { Name = "Искусство" };
            Tag tag4 = new Tag {  Name = "Manhattan" };
            Tag tag5 = new Tag { Name = "Корабли" };
            Tag tag6 = new Tag {  Name = "9-11" };
            Tag tag7 = new Tag { Name = "Математика" };
            Tag tag8 = new Tag { Name = "Президенты" };
            Tag tag9 = new Tag { Name = "WASP" };
            Tag tag10 = new Tag { Name = "Эмигранты" };
            Tag tag11 = new Tag { Name = "Юмор" };
            Tag tag12 = new Tag { Name = "Знаменитости" };
            Tag tag13 = new Tag { Name = "Ирландцы" };
            Tag tag14 = new Tag { Name = "Американцы" };
            Tag tag15 = new Tag { Name = "Культура" };
            Tag tag16 = new Tag { Name = "Архитектура" };
            Tag tag17 = new Tag { Name = "Эллис Айленд" };
            Tag tag18 = new Tag { Name = "Россия - Америка" };
            Tag tag19 = new Tag { Name = "Космос" };
            Tag tag20 = new Tag { Name = "Метро" };
           
            string postPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Content/UserPosts/", "");

            BlogPage aboutPage = new BlogPage {  Description = "About", Content = File.ReadAllText(postPath + "About.txt") };
            BlogPage contactPage = new BlogPage { Description = "Contact", Content = File.ReadAllText(postPath + "Contact.txt") };

            Post post1 = new Post
            {
                Id = 1,
                Title = "Манхэттен. Переобитаемый остров",
               
                Tags = new List<Tag> { tag2, tag4 },
                Description = File.ReadAllText(postPath + "ShortPost13-1.txt"),
                Body = File.ReadAllText(postPath + "Post13-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-9),
               
                Published = true
            };

            

            Post post2 = new Post
            {
                Id = 2,
                Title = "Манхэттен. Районы",
                Tags = new List<Tag> { tag2, tag4, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost13-2.txt"),
                Body = File.ReadAllText(postPath + "Post13-2.txt"),
                PostedOn = DateTime.Now.AddMonths(-5),
                Published = true
            };

            Post post3 = new Post
            {
                Id = 3,
                Title = "Манхэттен. История. Манна-хатта, Новый Амстердам, Нью-Йорк",
                Tags = new List<Tag> { tag2, tag4, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost13-3.txt"),
                Body = File.ReadAllText(postPath + "Post13-3.txt"),
                PostedOn = DateTime.Now.AddMonths(-1),
               
                Published = true
            };

            Post post4 = new Post
             {
                Id = 4,
                Title = "Достопримечательности Нью-Йорка: Метро. Искусство подземки. Masstransiscope",
                Tags = new List<Tag> { tag2, tag3, tag4, tag1 ,tag20},
                Description = File.ReadAllText(postPath + "ShortPost15-1.txt"),
                Body = File.ReadAllText(postPath + "Post15-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-1),                
                Published = true
            };

            Post post5 = new Post
             {
                Id = 5,
                Title = "Клипера, гончие морей",
                Tags = new List<Tag> { tag2, tag3, tag5 },
                Description = File.ReadAllText(postPath + "ShortPost1-1.txt"),
                Body = File.ReadAllText(postPath + "Post1-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),                
                Published = true
            };

            Post post6 = new Post
            {
                Id = 6,
                Title = "Американская история. 9/11. ВТЦ - от фундамента до Ground Zero",
                Tags = new List<Tag> { tag2, tag4, tag6, tag16, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost2-1.txt"),
                Body = File.ReadAllText(postPath + "Post2-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };

            Post post7 = new Post
            {
                Id = 7,
                Title = "Президенты. Джеймс Гарфилд. Президентское доказательство",
                Tags = new List<Tag> { tag2, tag7, tag8, tag12 },
                Description = File.ReadAllText(postPath + "ShortPost3-1.txt"),
                Body = File.ReadAllText(postPath + "Post3-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };

            Post post8 = new Post
            {
                Id = 8,
                Title = "Американцы. WASP. Жилища. Исторические дома",
                Tags = new List<Tag> { tag2, tag9, tag14, tag15, tag16 },
                Description = File.ReadAllText(postPath + "ShortPost4-1.txt"),
                Body = File.ReadAllText(postPath + "Post4-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            }; 
            
             Post post9 = new Post
            {
                Id = 9,
                Title = "Американцы. Стереотипы. Белые (янки, реднеки, хиллбилли и другие)",
                Tags = new List<Tag> { tag2, tag9 , tag14 },
                Description = File.ReadAllText(postPath + "ShortPost5-1.txt"),
                Body = File.ReadAllText(postPath + "Post5-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };

             Post post10 = new Post
             {
                 Id = 10,
                 Title = "Женщины Америки",
                 Tags = new List<Tag> { tag2,  tag12, tag14 },
                 Description = File.ReadAllText(postPath + "ShortPost6-1.txt"),
                 Body = File.ReadAllText(postPath + "Post6-1.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post11 = new Post
             {
                 Id = 11,
                 Title = "Эмигранты. Ирландцы. История",
                 Tags = new List<Tag> { tag2,  tag10, tag13 },
                 Description = File.ReadAllText(postPath + "ShortPost7-1.txt"),
                 Body = File.ReadAllText(postPath + "Post7-1.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post12 = new Post
             {
                 Id = 12,
                 Title = "Эмигранты. Ирландцы. Национальные особенности",
                 Tags = new List<Tag> { tag2,  tag10, tag13, tag15  },
                 Description = File.ReadAllText(postPath + "ShortPost7-2.txt"),
                 Body = File.ReadAllText(postPath + "Post7-2.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post13 = new Post
             {
                 Id = 13,
                 Title = "Эмигранты. Ирландцы. Анекдоты",
                 Tags = new List<Tag> { tag2,  tag10, tag11, tag13 },
                 Description = File.ReadAllText(postPath + "ShortPost7-3.txt"),
                 Body = File.ReadAllText(postPath + "Post7-3.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             
             Post post14 = new Post
             {
                 Id = 14,
                 Title = "Эмигранты. Откуда мы",
                 Tags = new List<Tag> { tag2, tag10 },
                 Description = File.ReadAllText(postPath + "ShortPost7-4.txt"),
                 Body = File.ReadAllText(postPath + "Post7-4.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };
             Post post15 = new Post
             {
                 Id = 15,
                 Title = "Эмигранты. От въезда до получения гражданства",
                 Tags = new List<Tag> { tag2, tag10  },
                 Description = File.ReadAllText(postPath + "ShortPost7-5.txt"),
                 Body = File.ReadAllText(postPath + "Post7-5.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };
             Post post16 = new Post
             {
                 Id = 16,
                 Title = "Эмигранты. Нелегалы",
                 Tags = new List<Tag> { tag2, tag10 },
                 Description = File.ReadAllText(postPath + "ShortPost7-6.txt"),
                 Body = File.ReadAllText(postPath + "Post7-6.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post17 = new Post
             {
                 Id = 17,
                 Title = "Эмигранты. Происхождение видов",
                 Tags = new List<Tag> { tag2, tag10 },
                 Description = File.ReadAllText(postPath + "ShortPost7-7.txt"),
                 Body = File.ReadAllText(postPath + "Post7-7.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post18 = new Post
             {
                 Id = 18,
                 Title = "Эмигранты. Стереотипы",
                 Tags = new List<Tag> { tag2, tag10},
                 Description = File.ReadAllText(postPath + "ShortPost7-8.txt"),
                 Body = File.ReadAllText(postPath + "Post7-8.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post19 = new Post
             {
                 Id = 19,
                 Title = "Эмигранты. Три иммиграции: старая, новая, новейшая",
                 Tags = new List<Tag> { tag2, tag10 },
                 Description = File.ReadAllText(postPath + "ShortPost7-9.txt"),
                 Body = File.ReadAllText(postPath + "Post7-9.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             

             Post post20 = new Post
             {
                 Id = 20,
                 Title = "Эмигранты. Нежелательные элементы",
                 Tags = new List<Tag> { tag2, tag10 },
                 Description = File.ReadAllText(postPath + "ShortPost7-10.txt"),
                 Body = File.ReadAllText(postPath + "Post7-10.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post21 = new Post
             {
                 Id = 21,
                 Title = "Эмигранты. Из ливерпульской гавани всегда по четвергам ...",
                 Tags = new List<Tag> { tag2, tag10 },
                 Description = File.ReadAllText(postPath + "ShortPost7-11.txt"),
                 Body = File.ReadAllText(postPath + "Post7-11.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post22 = new Post
             {
                 Id = 22,
                 Title = "Эмигранты. Steerage passengersя",
                 Tags = new List<Tag> { tag2, tag10 },
                 Description = File.ReadAllText(postPath + "ShortPost7-12.txt"),
                 Body = File.ReadAllText(postPath + "Post7-12.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post23 = new Post
             {
                 Id = 23,
                 Title = "Эмигранты. Эллис Айленд (1). На закат, где дрожат паруса",
                 Tags = new List<Tag> { tag2, tag10, tag17, tag1 },
                 Description = File.ReadAllText(postPath + "ShortPost7-13.txt"),
                 Body = File.ReadAllText(postPath + "Post7-13.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post24 = new Post
             {
                 Id = 24,
                 Title = "Эмигранты. Эллис Айленд (2). Остров Слез",
                 Tags = new List<Tag> { tag2, tag10, tag17, tag1 },
                 Description = File.ReadAllText(postPath + "ShortPost7-14.txt"),
                 Body = File.ReadAllText(postPath + "Post7-14.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

             Post post25 = new Post
             {
                 Id = 25,
                 Title = "Россия - Америка. Холодная война. Космическая гонка",
                 Tags = new List<Tag> { tag2, tag18, tag19 },
                 Description = File.ReadAllText(postPath + "ShortPost8-1.txt"),
                 Body = File.ReadAllText(postPath + "Post8-1.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

            Post post26 = new Post
             {
                 Id = 26,
                 Title = "Россия - Америка. Сравнительное жизнеописание. Deja vu",
                 Tags = new List<Tag> { tag2, tag18 },
                 Description = File.ReadAllText(postPath + "ShortPost8-2.txt"),
                 Body = File.ReadAllText(postPath + "Post8-2.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

            Post post27 = new Post
             {
                 Id = 27,
                 Title = "Жизнь в Нью-Йорке: американские \"непривычности\"",
                 Tags = new List<Tag> { tag2, tag18, tag1 },
                 Description = File.ReadAllText(postPath + "ShortPost8-3.txt"),
                 Body = File.ReadAllText(postPath + "Post8-3.txt"),
                 PostedOn = DateTime.Now.AddMonths(-4),
                 Published = true
             };

            Post post28 = new Post
            {
                Id = 28,
                Title = "Американская история. Ревущие двадцатые. Сухой закон",
                Tags = new List<Tag> { tag2, tag18 },
                Description = File.ReadAllText(postPath + "ShortPost9-1.txt"),
                Body = File.ReadAllText(postPath + "Post9-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };
            Post post29 = new Post
            {
                Id = 29,
                Title = "Достопримечательности Нью-Йорка: небоскребы",
                Tags = new List<Tag> { tag4, tag16, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost10-1.txt"),
                Body = File.ReadAllText(postPath + "Post10-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };

            
            Post post30 = new Post
            {
                Id = 30,
                Title = "Достопримечательности Нью-Йорка: мосты",
                Tags = new List<Tag> { tag4, tag16, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost10-2.txt"),
                Body = File.ReadAllText(postPath + "Post10-2.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };
            Post post31 = new Post
            {
                Id = 31,
                Title = "Брайтон, местечко у океана. Перечень постов",
                Tags = new List<Tag> { tag2, tag18, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost10-3.txt"),
                Body = File.ReadAllText(postPath + "Post10-3.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };
            Post post32 = new Post
            {
                Id = 32,
                Title = "Достопримечательности Нью-Йорка: Метро. История",
                Tags = new List<Tag> { tag2, tag4, tag20, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost11-1.txt"),
                Body = File.ReadAllText(postPath + "Post11-1.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };
            Post post33 = new Post
            {
                Id = 33,
                Title = "Достопримечательности Нью-Йорка: Метро. Сегодняшний день",
                Tags = new List<Tag> { tag2, tag20, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost11-2.txt"),
                Body = File.ReadAllText(postPath + "Post11-2.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };
            Post post34 = new Post
            {
                Id = 34,
                Title = "Достопримечательности Нью-Йорка: Метро. Искусство подземки ",
                Tags = new List<Tag> { tag4, tag3, tag20, tag1 },
                Description = File.ReadAllText(postPath + "ShortPost11-3.txt"),
                Body = File.ReadAllText(postPath + "Post11-3.txt"),
                PostedOn = DateTime.Now.AddMonths(-4),
                Published = true
            };

            var Comment1_1 = new Comment
            {
                CommentBody = "Comment1",
                UserName = "user1",
                Post = post1,
                PostedOn = DateTime.Now
            };

            var Comment1_2 = new Comment
            {
                CommentBody = "Comment2",
                UserName = "user2",
                ParentComment = Comment1_1,
                Post = post1,
                PostedOn = DateTime.Now
            };

            var Comment1_3 = new Comment
            {
                CommentBody = "Comment3",
                UserName = "user3",
                ParentComment = Comment1_2,
                Post = post1,
                PostedOn = DateTime.Now
            };

            var Comment1_4 = new Comment
            {
                CommentBody = "Comment4",
                UserName = "user4",
                ParentComment = Comment1_3,
                Post = post1,
                PostedOn = DateTime.Now
            };


            List<Tag> tags = new List<Tag>() { 
               tag1, 
               tag2, 
               tag3, 
               tag4,
               tag5,  tag6, tag7,  tag8, tag9, tag10, tag11,tag12, 
               tag13, tag14,  tag15, tag16, tag17,tag18, tag19, tag20,
            };

            List<Post> posts = new List<Post>(){
              post1, 
              post2, 
              post3, 
              post4,
              post5,
              post6,
              post7,
              post8,
              post9, 
              post10, 
              post11, post12, post13, post14,  post15,  post16,  post17,  post18,  post19, post20, post21, post22, post23, post24, 
              post25,  post26,  post27,
              post28,  post29,
              post30,  post31,
              post32,  post33,  post34
            };
           

            List<Comment> comments = new List<Comment>(){
              Comment1_1, 
              Comment1_2, 
              Comment1_3, 
              Comment1_4,               
            };
            post1.CommentsCount = 4;

            context.BlogPages.Add(aboutPage);
            context.BlogPages.Add(contactPage);
            context.Tags.AddRange(tags);
            context.Posts.AddRange(posts);
            context.Comments.AddRange(comments);

            context.SaveChanges();
        
        }
    }
}