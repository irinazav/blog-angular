﻿



$(document).ready(function () {
  
    //---------------------
    $('.pager .disabled a').on('click', function (e) {
        e.preventDefault();
    });

    //---------------------
    $(".container").on("click", ".link-can-not", function (e) {        
        e.preventDefault();

        var dialogInstance = new BootstrapDialog({
            title: 'Warning',
            type: BootstrapDialog.TYPE_WARNING,
            message: "Anonymous can't rate this post"
        });
        dialogInstance.open();
      
    });
    //---------------------

    $(document).tooltip({
        items: 'a.likecount',
        tooltipClass: 'preview-tip',
        position: { my: "left+5 top", at: "right center" },
        content: function (callback) {
            var $this = $(this);
           
            var href = $this.prev().attr("href")
                  .replace("AddPost", "GetNamesWho"); //GetNamesWho{Like/Dislike}/Id
                  
           

            $.post(href, function (data) {
                var userNamesWhoLike = "<div><span class=\"likeListheader\">" +
                       data.likeordislike + ":</span><br/>" +
                       (data.count ? data.usernames.join(",<br>") : "No names") +
                       (data.count > data.numbertoshow ? "<br/>and " + (data.count - data.numbertoshow) + " more ..." : "") +
                       "</div>";

                callback(userNamesWhoLike);
                if ($this.text() != data.count) $this.text(data.count);
            });
        },
    });

    //---------------------
    $(".container").on("click", ".link-like", function (event) {
        event.preventDefault();
        var $this = $(this);
        var href = $this.attr("href");
        
        $.post(href, function (data) {
            if (data.result == 0) {

                var dialogInstance = new BootstrapDialog({
                    title: 'Warning',
                    type: BootstrapDialog.TYPE_DANGER,
                    message: "Post rating can't be saved"
                });
                dialogInstance.open();
            }
            else if (data.result == -1) {

                var dialogInstance = new BootstrapDialog({
                    title: 'Warning',
                    type: BootstrapDialog.TYPE_WARNING,
                    message: "You can't rate this post twice"
                });
                dialogInstance.open();
            }

            else $this.next().text(data.result);
        });
    });

    //---------------------
    $(".container").on("click", ".link-tag", function (event) {
        event.preventDefault();
        var href = $(this).attr("href");
        var tagName = $(this).text();
        $.post(href, function (data) {
            data.TagName = tagName;
            var newControl = $("#tmpPosts").tmpl(data);           
            $("#main-content").empty().append(newControl);           
        });

    });

    $(".container").on("click", ".link-nav-by-tag", function (event) {
        event.preventDefault();
        var href = $(this).attr("href");
        var tagName = $("#tag-name").text();
        $.post(href, function (data) {
            data.TagName = tagName;
            var newControl = $("#tmpPosts").tmpl(data);
            $("#main-content").empty().append(newControl);
        });

    });

    //---------------------
    $(".container").on("click", ".link-post", function (event) {
        event.preventDefault();
        var href = $(this).attr("href");
        $.post(href, function (data) {
            var newControl = $("#tmpPost").tmpl(data.post);
            $("#main-content").empty().append(newControl);
        });
    });

    //---------------------
    $(".container").on("click", ".link-post-expand", function (event) {
        event.preventDefault();
        var href = $(this).attr("href");
        var $body = $(this).closest(".row").find(".post-body");
        if ($body.children().length == 0)
        {
            $.post(href, function (data) {
                $body.append(data.Body);
            });
        }
        else
        {
            $body.toggle();
        }
    });

    //---------------------     
})


